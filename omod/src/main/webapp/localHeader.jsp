<!-- <spring:htmlEscape defaultHtmlEscape="true" /> -->
<ul id="menu">
	<li class="first">
		<a href="${pageContext.request.contextPath}/admin"><spring:message code="admin.title.short" /></a>
	</li>

	<li
		<c:if test='<%= request.getRequestURI().contains("/manage") %>'>class="active"</c:if>>
		<a href="${pageContext.request.contextPath}/module/servicedelivery/manage.form">
			<spring:message	code="servicedelivery.manage" />
		</a>
	</li>
	<li
		<c:if test='<%= request.getRequestURI().contains("/template") %>'>class="active"</c:if>>
		<a href="${pageContext.request.contextPath}/module/servicedelivery/template.list">
			<spring:message	code="servicedelivery.template" />
		</a>
	</li>
	<li
		<c:if test='<%= request.getRequestURI().contains("/service.list") %>'>class="active"</c:if>>
		<a href="${pageContext.request.contextPath}/module/servicedelivery/service.list">
			<spring:message	code="servicedelivery.service" />
		</a>
	</li>
	<li
		<c:if test='<%= request.getRequestURI().contains("/package") %>'>class="active"</c:if>>
		<a href="${pageContext.request.contextPath}/module/servicedelivery/package.list">
			<spring:message	code="servicedelivery.package" />
		</a>
	</li>
	<!-- Add further links here -->
</ul>
<h2>
	<spring:message code="servicedelivery.title" />
</h2>
