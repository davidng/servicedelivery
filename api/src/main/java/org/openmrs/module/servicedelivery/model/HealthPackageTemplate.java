/**
 * 
 */
package org.openmrs.module.servicedelivery.model;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.User;

/**
 * @author upc26
 *
 */
public class HealthPackageTemplate implements java.io.Serializable{
	
	public static final long serialVersionUID = 1L;
	
	private static final Log log = LogFactory.getLog(HealthPackageTemplate.class);
	
	private Integer packageTemplateId;
	private String name;
	private Boolean voided;
	private Date dateVoided;
	private String voidReason;
	private String uuid;
	
	private User userVoid;
	/**
	 * @return the packageTemplateId
	 */
	public Integer getPackageTemplateId() {
		return packageTemplateId;
	}
	/**
	 * @param packageTemplateId the packageTemplateId to set
	 */
	public void setPackageTemplateId(Integer packageTemplateId) {
		this.packageTemplateId = packageTemplateId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the voided
	 */
	public Boolean getVoided() {
		return voided;
	}
	/**
	 * @param voided the voided to set
	 */
	public void setVoided(Boolean voided) {
		this.voided = voided;
	}
	/**
	 * @return the dateVoided
	 */
	public Date getDateVoided() {
		return dateVoided;
	}
	/**
	 * @param dateVoided the dateVoided to set
	 */
	public void setDateVoided(Date dateVoided) {
		this.dateVoided = dateVoided;
	}
	/**
	 * @return the voidReason
	 */
	public String getVoidReason() {
		return voidReason;
	}
	/**
	 * @param voidReason the voidReason to set
	 */
	public void setVoidReason(String voidReason) {
		this.voidReason = voidReason;
	}
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/**
	 * @return the userVoid
	 */
	public User getUserVoid() {
		return userVoid;
	}
	/**
	 * @param userVoid the userVoid to set
	 */
	public void setUserVoid(User userVoid) {
		this.userVoid = userVoid;
	}
}
