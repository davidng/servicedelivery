/**
 * 
 */
package org.openmrs.module.servicedelivery.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author upc26
 *
 */
public class HealthServiceCoverage implements java.io.Serializable{
	
	public static final long serialVersionUID = 1L;
	
	private static final Log log = LogFactory.getLog(HealthPackageTemplate.class);
	
	private Double unitPrice;
	private Integer quota;
	private Integer currentUse;
	private String uuid;
	
	private HealthPackage healthPackage;
	private HealthService healthService;
	/**
	 * @return the unitPrice
	 */
	public Double getUnitPrice() {
		return unitPrice;
	}
	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	/**
	 * @return the quota
	 */
	public Integer getQuota() {
		return quota;
	}
	/**
	 * @param quota the quota to set
	 */
	public void setQuota(Integer quota) {
		this.quota = quota;
	}
	/**
	 * @return the currentUse
	 */
	public Integer getCurrentUse() {
		return currentUse;
	}
	/**
	 * @param currentUse the currentUse to set
	 */
	public void setCurrentUse(Integer currentUse) {
		this.currentUse = currentUse;
	}
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/**
	 * @return the healthPackage
	 */
	public HealthPackage getHealthPackage() {
		return healthPackage;
	}
	/**
	 * @param healthPackage the healthPackage to set
	 */
	public void setHealthPackage(HealthPackage healthPackage) {
		this.healthPackage = healthPackage;
	}
	/**
	 * @return the healthService
	 */
	public HealthService getHealthService() {
		return healthService;
	}
	/**
	 * @param healthService the healthService to set
	 */
	public void setHealthService(HealthService healthService) {
		this.healthService = healthService;
	}
	
}
