/**
 * 
 */
package org.openmrs.module.servicedelivery.model;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.User;

/**
 * @author upc26
 *
 */
public class HealthPackage implements java.io.Serializable{
	
	public static final long serialVersionUID = 1L;
	
	private static final Log log = LogFactory.getLog(HealthPackageTemplate.class);
	
	private Integer packageId;
	private Date startDate;
	private Date expiryDate;
	private Short priority;
	private Boolean voided;
	private Date dateVoided;
	private String voidReason;
	
	private HealthPackageTemplate healthPackageTemplate;
	private Patient patient;
	private User userVoid;
	/**
	 * @return the packageId
	 */
	public Integer getPackageId() {
		return packageId;
	}
	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}
	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	/**
	 * @return the priority
	 */
	public Short getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Short priority) {
		this.priority = priority;
	}
	/**
	 * @return the voided
	 */
	public Boolean getVoided() {
		return voided;
	}
	/**
	 * @param voided the voided to set
	 */
	public void setVoided(Boolean voided) {
		this.voided = voided;
	}
	/**
	 * @return the dateVoided
	 */
	public Date getDateVoided() {
		return dateVoided;
	}
	/**
	 * @param dateVoided the dateVoided to set
	 */
	public void setDateVoided(Date dateVoided) {
		this.dateVoided = dateVoided;
	}
	/**
	 * @return the voidReason
	 */
	public String getVoidReason() {
		return voidReason;
	}
	/**
	 * @param voidReason the voidReason to set
	 */
	public void setVoidReason(String voidReason) {
		this.voidReason = voidReason;
	}
	/**
	 * @return the healthPackageTemplate
	 */
	public HealthPackageTemplate getHealthPackageTemplate() {
		return healthPackageTemplate;
	}
	/**
	 * @param healthPackageTemplate the healthPackageTemplate to set
	 */
	public void setHealthPackageTemplate(HealthPackageTemplate healthPackageTemplate) {
		this.healthPackageTemplate = healthPackageTemplate;
	}
	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}
	/**
	 * @param patient the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	/**
	 * @return the user
	 */
	public User getUserVoid() {
		return userVoid;
	}
	/**
	 * @param user the user to set
	 */
	public void setUserVoid(User user) {
		this.userVoid = user;
	}
}
