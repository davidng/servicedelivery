/**
 * 
 */
package org.openmrs.module.servicedelivery.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Obs;

/**
 * @author upc26
 *
 */
public class HealthServiceUsage implements java.io.Serializable{
	
	public static final long serialVersionUID = 1L;
	
	private static final Log log = LogFactory.getLog(HealthPackageTemplate.class);
	
	private Double unitPrice;
	private Integer quantity;
	
	private HealthPackage healthPackage;
	private HealthService healthService;
	private Obs obs;
	/**
	 * @return the unitPrice
	 */
	public Double getUnitPrice() {
		return unitPrice;
	}
	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the healthPackage
	 */
	public HealthPackage getHealthPackage() {
		return healthPackage;
	}
	/**
	 * @param healthPackage the healthPackage to set
	 */
	public void setHealthPackage(HealthPackage healthPackage) {
		this.healthPackage = healthPackage;
	}
	/**
	 * @return the healthService
	 */
	public HealthService getHealthService() {
		return healthService;
	}
	/**
	 * @param healthService the healthService to set
	 */
	public void setHealthService(HealthService healthService) {
		this.healthService = healthService;
	}
	/**
	 * @return the obs
	 */
	public Obs getObs() {
		return obs;
	}
	/**
	 * @param obs the obs to set
	 */
	public void setObs(Obs obs) {
		this.obs = obs;
	}
}
