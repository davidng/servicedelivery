/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.servicedelivery.api;

import java.util.List;

import org.openmrs.api.OpenmrsService;
import org.openmrs.module.servicedelivery.api.db.ServiceDeliveryDAO;
import org.openmrs.module.servicedelivery.model.HealthPackage;
import org.openmrs.module.servicedelivery.model.HealthPackageTemplate;
import org.openmrs.module.servicedelivery.model.HealthService;
import org.springframework.transaction.annotation.Transactional;

/**
 * This service exposes module's core functionality. It is a Spring managed bean which is configured in moduleApplicationContext.xml.
 * <p>
 * It can be accessed only via Context:<br>
 * <code>
 * Context.getService(ServiceDeliveryService.class).someMethod();
 * </code>
 * 
 * @see org.openmrs.api.context.Context
 */
@Transactional
public interface ServiceDeliveryService extends OpenmrsService {
     
	/*
	 * Add service methods here
	 * 
	 */
	
	public void setDao(ServiceDeliveryDAO dao) ;
	
	public ServiceDeliveryDAO getDao();
	
	public HealthService createHealthService(HealthService service);

	public void deleteHealthService(HealthService service);
	
	public List<HealthService> getAllHealthService();
	
	public List<HealthService> getActiveHealthService();
	
	public HealthService getHealthServiceById(Integer serviceId);

	
	public HealthPackageTemplate createHealthPackageTemplate(HealthPackageTemplate template);

	public void deleteHealthPackageTemplate(HealthPackageTemplate template);
	
	public List<HealthPackageTemplate> getAllHealthPackageTemplate();
	
	public List<HealthPackageTemplate> getActiveHealthPackageTemplate();
	
	public HealthPackageTemplate getHealthPackageTemplateById(Integer templateId);
	

	public HealthPackage createHealthPackage(HealthPackage healthPackage);

	public void deleteHealthPackage(HealthPackage healthPackage);
	
	public List<HealthPackage> getAllHealthPackage();
	
	public List<HealthPackage> getActiveHealthPackage();
	
	public HealthPackage getHealthPackageById(Integer pakageId);
	 
	
}