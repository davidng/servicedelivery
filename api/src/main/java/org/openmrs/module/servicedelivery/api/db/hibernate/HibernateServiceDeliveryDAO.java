/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.servicedelivery.api.db.hibernate;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.openmrs.module.servicedelivery.api.db.ServiceDeliveryDAO;
import org.openmrs.module.servicedelivery.model.HealthPackage;
import org.openmrs.module.servicedelivery.model.HealthPackageTemplate;
import org.openmrs.module.servicedelivery.model.HealthService;

/**
 * It is a default implementation of  {@link ServiceDeliveryDAO}.
 */
public class HibernateServiceDeliveryDAO implements ServiceDeliveryDAO {
	protected final Log log = LogFactory.getLog(this.getClass());
	
	private SessionFactory sessionFactory;
	
	/**
     * @param sessionFactory the sessionFactory to set
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
	    this.sessionFactory = sessionFactory;
    }

	@Override
	public HealthService createHealthService(HealthService service) {
		// TODO Auto-generated method stub
		HealthService healthService = service;
		sessionFactory.getCurrentSession().saveOrUpdate(healthService);
		return healthService;
	}

	@Override
	public void deleteHealthService(HealthService service) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(service);
	}

	@Override
	public List<HealthService> getAllHealthService() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(HealthService.class).list();
	}

	@Override
	public List<HealthService> getActiveHealthService() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession()
		.createQuery("FROM HealthService AS a WHERE a.voided <> 0").list();
	}

	@Override
	public HealthService getHealthServiceById(Integer serviceId) {
		// TODO Auto-generated method stub
		return (HealthService) sessionFactory.getCurrentSession()
		.createQuery("From HealthService AS a WHERE a.serviceId = '" + serviceId + "'").uniqueResult();
	}

	@Override
	public HealthPackageTemplate createHealthPackageTemplate(HealthPackageTemplate template) {
		// TODO Auto-generated method stub
		HealthPackageTemplate healthServiceTemplate = template;
		sessionFactory.getCurrentSession().saveOrUpdate(healthServiceTemplate);
		return healthServiceTemplate;
	}

	@Override
	public void deleteHealthPackageTemplate(HealthPackageTemplate template) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(template);
	}

	@Override
	public List<HealthPackageTemplate> getAllHealthPackageTemplate() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createCriteria(HealthPackageTemplate.class).list();
	}

	@Override
	public List<HealthPackageTemplate> getActiveHealthPackageTemplate() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession()
		.createQuery("FROM HealthPackageTemplate AS a WHERE a.voided <> 0").list();
	}

	@Override
	public HealthPackageTemplate getHealthPackageTemplateById(Integer templateId) {
		// TODO Auto-generated method stub
		return (HealthPackageTemplate)sessionFactory.getCurrentSession()
		.createQuery("FROM HealthPackageTemplate AS a WHERE a.templateId = '" + templateId + "'").uniqueResult();
	}

	@Override
	public HealthPackage createHealthPackage(HealthPackage healthPackage) {
		// TODO Auto-generated method stub
		HealthPackage pack = healthPackage;
		sessionFactory.getCurrentSession().saveOrUpdate(pack);
		return pack;
	}

	@Override
	public void deleteHealthPackage(HealthPackage healthPackage) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(healthPackage);
	}

	@Override
	public List<HealthPackage> getAllHealthPackage() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession()
		.createCriteria(HealthPackage.class).list();
	}

	@Override
	public List<HealthPackage> getActiveHealthPackage() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession()
		.createQuery("FROM HealthPackage AS a WHERE a.voide <> 0").list();
	}

	@Override
	public HealthPackage getHealthPackageById(Integer packageId) {
		// TODO Auto-generated method stub
		return (HealthPackage) sessionFactory.getCurrentSession()
		.createQuery("FROM HealthPackage AS a WHERE a.packageId = '" + packageId + "'").uniqueResult();
	}
    
}