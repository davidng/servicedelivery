package org.openmrs.module.servicedelivery.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HealthPackageTemplateService implements java.io.Serializable{
	
	public static final long serialVersionUID = 1L;
	
	private static final Log log = LogFactory.getLog(HealthPackageTemplate.class);
	
	private Double unitPrice;
	private Integer quota;
	private String uuid;
	
	private HealthPackageTemplate healthPackageTemplate;
	private HealthService healthService;
	/**
	 * @return the unitPrice
	 */
	public Double getUnitPrice() {
		return unitPrice;
	}
	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	/**
	 * @return the quota
	 */
	public Integer getQuota() {
		return quota;
	}
	/**
	 * @param quota the quota to set
	 */
	public void setQuota(Integer quota) {
		this.quota = quota;
	}
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/**
	 * @return the healthPackageTemplate
	 */
	public HealthPackageTemplate getHealthPackageTemplate() {
		return healthPackageTemplate;
	}
	/**
	 * @param healthPackageTemplate the healthPackageTemplate to set
	 */
	public void setHealthPackageTemplate(HealthPackageTemplate healthPackageTemplate) {
		this.healthPackageTemplate = healthPackageTemplate;
	}
	/**
	 * @return the healthService
	 */
	public HealthService getHealthService() {
		return healthService;
	}
	/**
	 * @param healthService the healthService to set
	 */
	public void setHealthService(HealthService healthService) {
		this.healthService = healthService;
	}
	
}
