/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.servicedelivery.api.impl;

import java.util.List;

import org.openmrs.api.impl.BaseOpenmrsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.servicedelivery.api.ServiceDeliveryService;
import org.openmrs.module.servicedelivery.api.db.ServiceDeliveryDAO;
import org.openmrs.module.servicedelivery.model.HealthPackage;
import org.openmrs.module.servicedelivery.model.HealthPackageTemplate;
import org.openmrs.module.servicedelivery.model.HealthService;

/**
 * It is a default implementation of {@link ServiceDeliveryService}.
 */
public class ServiceDeliveryServiceImpl extends BaseOpenmrsService implements ServiceDeliveryService {
	
	protected final Log log = LogFactory.getLog(this.getClass());
	
	private ServiceDeliveryDAO dao;
	
	
	/**
	 * @return the dao
	 */
	public ServiceDeliveryDAO getDao() {
		return dao;
	}

	/**
	 * @param dao the dao to set
	 */
	public void setDao(ServiceDeliveryDAO dao) {
		this.dao = dao;
	}

	public HealthService createHealthService(HealthService service)
	{
		return dao.createHealthService(service);
	}

	public void deleteHealthService(HealthService service)
	{
		dao.deleteHealthService(service);
	}
	
	public List<HealthService> getAllHealthService()
	{
		return dao.getAllHealthService();
	}
	
	public List<HealthService> getActiveHealthService()
	{
		return dao.getActiveHealthService();
	}
	
	public HealthService getHealthServiceById(Integer serviceId)
	{
		return dao.getHealthServiceById(serviceId);
	}

	
	public HealthPackageTemplate createHealthPackageTemplate(HealthPackageTemplate template)
	{
		return dao.createHealthPackageTemplate(template);
	}

	public void deleteHealthPackageTemplate(HealthPackageTemplate template)
	{
		dao.deleteHealthPackageTemplate(template);
	}
	
	public List<HealthPackageTemplate> getAllHealthPackageTemplate()
	{
		return dao.getAllHealthPackageTemplate();
	}
	
	public List<HealthPackageTemplate> getActiveHealthPackageTemplate()
	{
		return dao.getActiveHealthPackageTemplate();
	}
	
	public HealthPackageTemplate getHealthPackageTemplateById(Integer templateId)
	{
		return dao.getHealthPackageTemplateById(templateId);
	}
	

	public HealthPackage createHealthPackage(HealthPackage healthPackage)
	{
		return dao.createHealthPackage(healthPackage);
	}

	public void deleteHealthPackage(HealthPackage healthPackage)
	{
		dao.deleteHealthPackage(healthPackage);
	}
	
	public List<HealthPackage> getAllHealthPackage()
	{
		return dao.getAllHealthPackage();
	}
	
	public List<HealthPackage> getActiveHealthPackage()
	{
		return dao.getActiveHealthPackage();
	}
	
	public HealthPackage getHealthPackageById(Integer pakageId)
	{
		return dao.getHealthPackageById(pakageId);
	}
}